library(shiny);
library(bslib);
library(ggplot2);
library(gridExtra); #pour arranger les graphiques


LAImax_min <- 1.5;
LAImax_max <- 10.;
LAImax_step <- 0.1;
LAImax_default <- 4.5;
valeurs_LAImax_value <- seq(from = LAImax_min, to = LAImax_max, by = LAImax_step);
NBvaleurs_LAImax_value <- length(valeurs_LAImax_value);

RUE_min <- 0.01;
RUE_max <- 0.05;
RUE_step <- 0.0005;
RUE_default <- 0.022;
valeurs_RUE_value <- seq(from = RUE_min, to = RUE_max, by = RUE_step);
NBvaleurs_RUE_value <- length(valeurs_RUE_value);


load("www/TP3.RData"); # provide simulation_result_list

indice_list <- matrix(1:(NBvaleurs_LAImax_value * NBvaleurs_RUE_value),
    nrow = NBvaleurs_RUE_value, ncol = NBvaleurs_LAImax_value,
    dimnames = list(valeurs_RUE_value, valeurs_LAImax_value));

simulation_by_rdata <- function(LAImax_value, RUE_value)
{
  return(simulation_result_list[[indice_list[as.character(RUE_value), as.character(LAImax_value)]]]);
}
